package main

type CorruptReferee struct {
	Referee
}

func (CorruptReferee) Decide(event Event) Decision {
	return Decision{infraction: "Cool", interpretation: "Everything is Fine"}
}
