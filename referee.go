package main

// Referre is an abstract concept, or in other terms
// the idea of an abstract class
type Referee struct {
	id string
	name string
	country Country
}
